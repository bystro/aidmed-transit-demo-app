$('#report-daily').click(function (e) {
    error = false;
    e.preventDefault();
    if (
            $.trim($('#report_start_date').val()) === ""
            || $.trim($('#report_end_date').val()) === ""
            ) {
        $('#report-daily-error').html('Wypełnij oba pola z datą');
        $('#report-daily-error').show();
        return;
    }

    $('#report-daily-error').hide();

    $.ajax({
        url: "http://54.37.235.139:90/reports/daily?start_date=" + $('#report_start_date').val() + "&end_date=" + $('#report_end_date').val(),
        method: "GET",
        crossDomain: true
    }).done(function (response, textStatus, responseHeader) {

        if (responseHeader.status === 200) {
            $('#report-daily-results-total-distance').html(response.total_distance);
            $('#report-daily-results-total-price').html(response.total_price);
            $('#reportDailyModal').modal('show');
        } else if (responseHeader.status === 202) {
            $('#report-daily-error').html('Dane dotyczące przebytego są w trakcie przeliczania. Spróbuj jeszcze raz.');
            $('#report-daily-error').show();
        } else {
            $('#report-daily-error').html('Wystąpił błąd. Spróbuj jeszcze raz.');
            $('#report-daily-error').show();
        }
    });

});


$('#report-monthly').click(function (e) {
    error = false;
    e.preventDefault();

    $('#report-monthly-error').hide();

    $.ajax({
        url: "http://54.37.235.139:90/reports/monthly",
        method: "GET",
        crossDomain: true
    }).done(function (response, textStatus, responseHeader) {

        if (responseHeader.status === 200) {

            $('#report-monthly-results-table-body').html('');
            response.forEach(function (e) {
                $('#report-monthly-results-table-body').append(
                        '<tr><td>' + e.date + '</td><td>' + e.total_distance + '</td><td>' + e.avg_distance + '</td><td>' + e.avg_price + '</td></tr>'
                        );
            });

            $('#reportMonthlyModal').modal('show');

        } else {
            $('#report-monthly-error').html('Wystąpił błąd. Spróbuj jeszcze raz.');
            $('#report-monthly-error').show();
        }
    });

});