$('#transit-create').click(function (e) {
    error = false;
    e.preventDefault();
    if (
            $.trim($('#source_address').val()) === ""
            || $.trim($('#destination_address').val()) === ""
            || $.trim($('#price').val()) === ""
            || $.trim($('#date').val()) === ""
            ) {
        $('#transit-create-validation-error').show();
        return;
    }

    $('#transit-create-validation-error').hide();
    $('#transit-create-success').hide();
    $('#transit-create-error').hide();

    var data = $('#transit-create-form').serialize();

    $.ajax({
        url: "http://54.37.235.139:90/transits",
        method: "POST",
        crossDomain: true,
        data: data
    }).done(function (response, textStatus, responseHeader) {

        if (responseHeader.status === 201) {
            $('#transit-create-success').show();

        } else {
            $('#transit-create-error').show();
        }
    });


});