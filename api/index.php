<?php
use Silex\Provider\DoctrineServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

require_once 'vendor/autoload.php';

$app = new Silex\Application();

require_once "config/config.php";
require_once "src/functions.php";

$app->register(new DoctrineServiceProvider(), $app['db.options']);
$app->register(new Silex\Provider\ValidatorServiceProvider());

$app->post('/transits', function (Request $request) use ($app) {
    $constraint = new Assert\Collection([
        'source_address'      => [
            new Assert\NotNull(),
            new Assert\NotBlank(),
        ],
        'destination_address' => [
            new Assert\NotNull(),
            new Assert\NotBlank(),
        ],
        'price'               => [
            new Assert\Type('integer'),
            new Assert\NotBlank(),
        ],
        'date'                => [
            new Assert\Date(),
            new Assert\NotBlank(),
        ],
    ]);

    $errors = $app['validator']->validate([
        'source_address'      => $request->request->get('source_address'),
        'destination_address' => $request->request->get('destination_address'),
        'date'                => $request->request->get('date'),
        'price'               => (int) $request->request->get('price'),
    ], $constraint);

    if (count($errors) > 0) {
        $response;
        foreach ($errors as $error) {
            $response[str_replace(array('[', ']'), '', $error->getPropertyPath())] = $error->getMessage();
        }

        return $app->json($response, 400);

    } else {

        $data = [
            'transit_from'  => $request->request->get('source_address'),
            'transit_to'    => $request->request->get('destination_address'),
            'transit_date'  => $request->request->get('date'),
            'transit_price' => (int) $request->request->get('price'),
        ];

        if ($app['db']->insert('transits', $data)) {
            return $app->json([], 201);
        } else {
            return $app->json([], 500);
        }

    }

});

$app->get('/reports/daily', function (Request $request) use ($app) {
	$constraint = new Assert\Collection([
        'start_date' => [
            new Assert\Date(),
            new Assert\NotBlank(),
        ],
        'end_date' => [
            new Assert\Date(),
            new Assert\NotBlank(),
        ],
    ]);

    $errors = $app['validator']->validate([
        'start_date'      => $request->query->get('start_date'),
        'end_date' => $request->query->get('end_date'),
    ], $constraint);

    if (count($errors) > 0) {
        $response;
        foreach ($errors as $error) {
            $response[str_replace(array('[', ']'), '', $error->getPropertyPath())] = $error->getMessage();
        }

        return $app->json($response, 400);

    } else {

	    $data = [
	        $request->query->get('start_date'),
	        $request->query->get('end_date'),
	    ];

	    $query = 'SELECT COUNT(*) FROM transits WHERE transit_date >= ? AND transit_date <= ? AND transit_distance IS NULL';

	    if ($app['db']->fetchColumn($query, $data, 0) > 0) {
	        return $app->json([], 202);
	    }

	    $query = 'SELECT SUM(transit_price) as total_price, SUM(transit_distance) as total_distance FROM transits WHERE transit_date >= ? AND transit_date <= ?';

	    $result = $app['db']->fetchAssoc($query, $data);

	    return $app->json($result, 200);
	}
});

$app->get('/reports/monthly', function (Request $request) use ($app) {
    $dates = [];

    $yearAndMonth = date('Y-m-');

    for ($i = 1; $i < date('j'); $i++) {
        $dates[] = $yearAndMonth . getTwoDigitsMonth($i);
    }

    $query = 'SELECT SUM(transit_price) as total_price, SUM(transit_distance) as total_distance, COUNT(*) as count, transit_date FROM transits WHERE transit_date >= ? AND transit_date <= ? GROUP BY transit_date';

    $results = $app['db']->fetchAll($query, [$dates[0], $dates[count($dates) - 1]]);

    $data = [];

    foreach ($results as $result) {
        $data[$result['transit_date']] = [
            'date'           => date('F, jS', strtotime($result['transit_date'])),
            'total_distance' => $result['total_distance'],
            'avg_distance'   => $result['total_distance'] / $result['count'],
            'avg_price'      => $result['total_price'] / $result['count'],
        ];
    }

    $respone = [];

    foreach ($dates as $date) {
        if (isset($data[$date])) {
            $response[] = $data[$date];
        } else {
            $response[] = [
                'date'           => date('F, jS', strtotime($date)),
                'total_distance' => 0,
                'avg_distance'   => 0,
                'avg_price'      => 0,
            ];
        }
    }

    return $app->json($response, 200);
});

$app->get('/update/distance', function () use ($app) {

    $query = 'SELECT transit_id, transit_from, transit_to FROM transits WHERE transit_distance IS NULL';

    $updateQuery = 'UPDATE transits SET transit_distance = ? WHERE transit_id = ?';

    foreach ($app['db']->fetchAll($query) as $transit) {
        $cordFrom = getCoordinates($transit['transit_from']);
        $cordTo   = getCoordinates($transit['transit_to']);

        if (!$cordFrom || !$cordTo) {
            $app['db']->executeUpdate($updateQuery, array(0, $transit['transit_id']));
        } else {
            $result = getDrivingDistance($cordFrom['lat'], $cordTo['lat'], $cordFrom['long'], $cordTo['long']);
            if ($result != null) {
                $app['db']->executeUpdate($updateQuery, array(substr($result, 0, -3), $transit['transit_id']));
            } else {
                $app['db']->executeUpdate($updateQuery, array(0, $transit['transit_id']));
            }
        }
    }

    return $app->json([], 200);

});

$app->run();
